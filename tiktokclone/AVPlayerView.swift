//
//  AVPlayerView.swift
//  tiktokclone
//
//  Created by PT.Koanba on 18/01/22.
//

import UIKit
import AVFoundation

final class AVPlayerView: UIView {

    override class var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
}

