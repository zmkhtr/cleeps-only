//
//  KipasPlayer.swift
//  KipasKipas
//
//  Created by PT.Koanba on 01/11/21.
//  Copyright © 2021 Koanba. All rights reserved.
//

import AVKit
import UIKit

protocol KipasPlayerDelegate {
    func kipasPlayerStateObserver(_ state: KipasPlayerState)
}

enum KipasPlayerState {
    case playing
    case paused
    case loading
    case ready
    case failed(error: String)
    case unknown
    case stop
}

final class KipasPlayer: UIView {
    
    private var looper: AVPlayerLooper?
    private var queuePlayer: AVQueuePlayer?
    private let playerLayer = AVPlayerLayer()
    var delegate: KipasPlayerDelegate!
    
    private var downloader = HLSDownloader.instance
    
    // Key-value observing context
    private var playerItemContext = 0

    private var url : String!
    private var width : Double!
    private var height : Double!
    
    let requiredAssetKeys = [
        "playable",
        "hasProtectedContent"
    ]
    
    private var isPlaying: Bool {
        queuePlayer?.rate != 0
    }
    
    override var isUserInteractionEnabled: Bool {
        get { true }
        set {}
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    private func setupSelf() {
        backgroundColor = .clear
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(didReceiveTap))
        addGestureRecognizer(tapRecognizer)
        
        playerLayer.videoGravity = .resizeAspectFill
        layer.addSublayer(playerLayer)
    }
    
    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        
        playerLayer.frame = layer.bounds
    }
    
    func prepareVideo(url: String, width: Double, height: Double) {
        self.url = url
        self.width = width
        self.height = height
        if width > height || width == height {
            playerLayer.videoGravity = .resizeAspect
        } else {
            playerLayer.videoGravity = .resizeAspectFill
        }
//        downloader.playOfflineAssetWithFallbackIfNotAvailable(url: url) { [weak self] playerItem in
        
        let asset = AVURLAsset(url: URL(string: url)!)
        let playerItem = AVPlayerItem(asset: asset)
            self.setPlayer(with: playerItem)
//        }
        
        downloader.bindError = { [weak self] error in
            self?.delegate.kipasPlayerStateObserver(.failed(error: error.localizedDescription))
            self?.recoverFromError()
        }
    }
    
    private func setPlayer(with playerItem: AVPlayerItem) {
//        playerItem.addObserver(self,
//                                 forKeyPath: #keyPath(AVPlayerItem.status),
//                                 options: [.old, .new],
//                                 context: &playerItemContext)
//        playerItem.addObserver(self, forKeyPath: "playbackBufferEmpty",
//                                    options: [.old, .new],
//                                    context: &playerItemContext)
//        playerItem.addObserver(self, forKeyPath: "playbackLikelyToKeepUp",
//                                    options: [.old, .new],
//                                    context: &playerItemContext)
//        playerItem.addObserver(self, forKeyPath: "playbackBufferFull",
//                                    options: [.old, .new],
//                                    context: &playerItemContext)
        
        let player = AVQueuePlayer(playerItem: playerItem)
        queuePlayer = player
        playerLayer.player = queuePlayer
        
        looper = AVPlayerLooper(player: player, templateItem: playerItem)
    }
    
    @objc
    private func didReceiveTap() {
        togglePlay()
    }
    
    func togglePlay(on: Bool? = nil) {
        if let on = on {
            on ? play(): pause()
        } else if isPlaying {
            pause()
        } else {
            play()
        }
    }
    
    func play(){
        queuePlayer?.play()
        delegate.kipasPlayerStateObserver(.playing)
//        downloader.resume()
    }
    
    func pause(){
        queuePlayer?.pause()
        delegate.kipasPlayerStateObserver(.paused)
//        downloader.suspend()
    }
    
    func stop(){
        queuePlayer?.pause()
        queuePlayer?.seek(to: .zero)
        delegate.kipasPlayerStateObserver(.stop)
//        downloader.cancel()
    }
    
    private func recoverFromError() {
        queuePlayer = nil
        queuePlayer?.replaceCurrentItem(with: nil)
        playerLayer.player = nil
        playerLayer.player?.replaceCurrentItem(with: nil)
        prepareVideo(url: self.url, width: self.width, height: self.height)
    }
}

// MARK: AVPlayer Observer
extension KipasPlayer {
    
    override func observeValue(forKeyPath keyPath: String?,
                               of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?,
                               context: UnsafeMutableRawPointer?) {

        // Only handle observations for the playerItemContext
        guard context == &playerItemContext else {
            super.observeValue(forKeyPath: keyPath,
                               of: object,
                               change: change,
                               context: context)
            return
        }

        if keyPath == #keyPath(AVPlayerItem.status) {
            let status: AVPlayerItem.Status
            if let statusNumber = change?[.newKey] as? NSNumber {
                status = AVPlayerItem.Status(rawValue: statusNumber.intValue)!
            } else {
                status = .unknown
            }

            // Switch over status value
            switch status {
            case .readyToPlay:
                // Player item is ready to play.
                self.delegate.kipasPlayerStateObserver(.ready)
            case .failed:
                // Player item failed. See error.
                self.delegate.kipasPlayerStateObserver(.failed(error: "Item Failed to Load"))
                self.recoverFromError()
            case .unknown:
                // Player item is not yet ready.
                self.delegate.kipasPlayerStateObserver(.unknown)
            @unknown default:
                fatalError("Error unknown")
            }
        }
        
        if object is AVPlayerItem {
            switch keyPath {
                case "playbackBufferEmpty":
                   // Show loader
                self.delegate.kipasPlayerStateObserver(.loading)

                case "playbackLikelyToKeepUp":
                    // Hide loader
                self.delegate.kipasPlayerStateObserver(.ready)

                case "playbackBufferFull":
                    // Hide loader
                self.delegate.kipasPlayerStateObserver(.ready)
            default:
                self.delegate.kipasPlayerStateObserver(.unknown)
            }
        }
    }
}
