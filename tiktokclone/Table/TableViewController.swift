//
//  TableViewController.swift
//  tiktokclone
//
//  Created by PT.Koanba on 18/01/22.
//

import AVKit
import UIKit

class TableViewVideoController : UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var data = DataProvider.data
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "CustomTableCell", bundle: nil), forCellReuseIdentifier: "tableIdentifier")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = self.tableView.frame.size.height
        tableView.estimatedRowHeight = self.tableView.frame.size.height
//        tableView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        togglePlayVisibleCell(play: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        togglePlayVisibleCell(play: true)
    }
    
    private func togglePlayVisibleCell(play: Bool) {
        tableView.visibleCells.forEach { cell in
            guard let cell = cell as? TableCell else { return }
            if play {
                cell.play()
                return
            }
            cell.pause()
        }
    }
}

extension TableViewVideoController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "tableIdentifier", for: indexPath) as? TableCell else { fatalError("Cannot dequeue cell") }
        
        cell.setUpVideoCell(url: data[indexPath.row])
        if indexPath.row == 0 {
            cell.play()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? TableCell else { return }
        cell.pause()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if  indexPath.row == self.data.count - 1  {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.data.append(contentsOf: DataProvider.randomData)
                print("ROW NYA \(indexPath.row) COUNTNYA \(self.data.count)")
                tableView.performBatchUpdates {
                    for i in self.data.count - 10...self.data.count - 1 {
                        print("I NYA \(i)")
                        self.tableView.insertRows(at: [IndexPath(row: i, section: 0)], with: .none)
                    }
                }
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for cell in tableView.visibleCells {
            if let cell = cell as? TableCell {
                cell.play()
            }
        }
    }
}


class TableCell : UITableViewCell {
  
    @IBOutlet weak var videoLayer: UIView!
    
    var queuePlayer: AVQueuePlayer!
    var playerLooper: AVPlayerLooper!
    var playerLayer: AVPlayerLayer!
    var playerItem: AVPlayerItem!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        playerLayer.frame = self.contentView.bounds
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        setupViewAVPlayerLayer()
        
    }
    
    func setUpVideoCell(url: String) {
        let asset = AVURLAsset(url: URL(string: url)!)

        self.playerItem = AVPlayerItem(asset: asset)
        if let queuePlayer = self.queuePlayer {
            queuePlayer.replaceCurrentItem(with: playerItem)
            self.playerLooper = AVPlayerLooper(player: queuePlayer, templateItem: playerItem)
            setupViewAVPlayerLayer()
        } else {
            self.queuePlayer = AVQueuePlayer(playerItem: playerItem)
            self.playerLooper = AVPlayerLooper(player: queuePlayer, templateItem: playerItem)
            setupViewAVPlayerLayer()
        }
//        self.queuePlayer.play()
        
        playerLayer.videoGravity = .resizeAspect
        playerLayer.contentsGravity = .resizeAspect
        contentView.sendSubviewToBack(self.videoLayer)

    }
    
    func play(){
        self.queuePlayer.play()
    }
    
    func pause(){
        self.queuePlayer.pause()
    }
    
    func setupViewAVPlayerLayer() {
        playerLayer = AVPlayerLayer(player: queuePlayer)
        playerLayer.frame = self.videoLayer.bounds
        playerLayer.videoGravity = .resizeAspect
        playerLayer.contentsGravity = .resizeAspect
        self.videoLayer.layer.addSublayer(self.playerLayer)
    }

}
