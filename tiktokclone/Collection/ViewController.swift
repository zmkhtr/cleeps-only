//
//  ViewController.swift
//  tiktokclone
//
//  Created by PT.Koanba on 09/01/22.
//

import UIKit
import AVKit

class ViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var data : [String] = DataProvider.data
    
    var currentCell : CollectionCell?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        collectionView.register(UINib(nibName: "CustomCell", bundle: nil), forCellWithReuseIdentifier: "identifier")
        collectionView.delegate = self
        collectionView.dataSource = self
//        collectionView.visibleSize = CGSize(width: collectionView.contentSize.width, height: collectionView.contentSize.height
//        collectionView.contentSize = CGSize(width: collectionView.bounds.width, height: collectionView.contentSize.height)
//        collectionView.reloadData()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        togglePlayVisibleCell(play: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        togglePlayVisibleCell(play: true)
    }
    
    private func togglePlayVisibleCell(play: Bool) {
        collectionView.visibleCells.forEach { cell in
            guard let cell = cell as? CollectionCell else { return }
            if play {
                cell.play()
                return
            }
            cell.pause()
        }
    }

    override func didReceiveMemoryWarning() {
        print("MEMORY WARNING")
    }
}



extension ViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "identifier", for: indexPath) as? CollectionCell else { fatalError("Cannot dequeue cell") }
        
        cell.setUpVideoCell(url: data[indexPath.row])
        
        if indexPath.row == 0 {
            cell.play()
            currentCell = cell
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if  indexPath.row == self.data.count - 1  {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.data.append(contentsOf: DataProvider.randomData)
//                self.collectionView.reloadData()
//                self.currentCell?.play()
                print("ROW NYA \(indexPath.row) COUNTNYA \(self.data.count)")
                collectionView.performBatchUpdates {
                    for i in self.data.count - 10...self.data.count - 1 {
                        print("I NYA \(i)")
                self.collectionView.insertItems(at: [IndexPath(row: i, section: 0)])
                    }
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? CollectionCell {
            cell.stop()
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for cell in collectionView.visibleCells {
            if let cell = cell as? CollectionCell {
                cell.play()
                currentCell = cell
            }
        }
    }
}

class CollectionCell : UICollectionViewCell {
    
    @IBOutlet weak var playerView: UIView!
//    @IBOutlet weak var kipasPlayer: KipasPlayer!

    lazy var kipasPlayer : KipasPlayer = {
       return KipasPlayer()
    }()
    var url = ""
    // Key-value observing context
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.playerView.addSubview(kipasPlayer)
        kipasPlayer.frame = self.contentView.frame
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        kipasPlayer.delegate = self
        
    }
    
    func setUpVideoCell(url: String) {
        self.url = url
        kipasPlayer.prepareVideo(url: url, width: 100, height: 100)
    }
  
    func stop(){
        self.kipasPlayer.stop()
    }
    
    func pause(){
        self.kipasPlayer.togglePlay(on: false)
    }
    
    func play(){
        self.kipasPlayer.togglePlay(on: true)
    }
  
}

extension CollectionCell : KipasPlayerDelegate {
    func kipasPlayerStateObserver(_ state: KipasPlayerState) {
        print("STATE \(state) URL \(url)")
//        switch state {
//        case .playing:
//
//        case .paused:
//
//        case .loading:
//
//        case .ready:
//
//        case .failed(let error):
//
//        case .unknown:
//
//        case .stop:
//
//        }
    }
}
