//
//  PageViewController.swift
//  tiktokclone
//
//  Created by PT.Koanba on 18/01/22.
//

import UIKit
import AVKit
import AVFoundation

class PageViewController : UIPageViewController {
    
    private(set) lazy var orderedViewControllers = [AVPlayerViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DataProvider.jwData.forEach { url in
            orderedViewControllers.append(createAVPlayerViewController(videoURL: url))
        }
        
        dataSource = self
        delegate = self
        
       
        
        if let firstViewController = orderedViewControllers.first {
              setViewControllers([firstViewController],
                 direction: .forward,
                  animated: true,
                  completion: nil)
          }
        
      
    }
    
    func createAVPlayerViewController(videoURL: String) -> VideoController {
        let videoURL = URL(string: videoURL)!
        
        let asset = AVURLAsset(url: videoURL)
        let playerItem = AVPlayerItem(asset: asset)
        let player = AVQueuePlayer(playerItem: playerItem)
        let _ = AVPlayerLooper(player: player, templateItem: playerItem)
        
        let playerViewController = VideoController()
        playerViewController.videoGravity = .resizeAspectFill
        playerViewController.player = player
        return playerViewController
    }
    
    @objc func playerItemDidReachEnd(notification: Notification) {
        if let playerItem = notification.object as? AVPlayerItem, let player = notification.object as? AVPlayer {
            playerItem.seek(to: CMTime.zero, completionHandler: nil)
            player.play()
        }
    }
    
}

extension PageViewController : UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController as! AVPlayerViewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController as! AVPlayerViewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    
    
}
