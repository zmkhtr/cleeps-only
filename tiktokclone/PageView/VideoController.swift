//
//  VideoController.swift
//  tiktokclone
//
//  Created by PT.Koanba on 18/01/22.
//

import UIKit
import AVKit

class VideoController : AVPlayerViewController {
    
    var size : (Double, Double)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.videoGravity = .resizeAspectFill
        self.player?.actionAtItemEnd = .none
        self.showsPlaybackControls = false
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerItemDidReachEnd(notification:)),
                                               name: .AVPlayerItemDidPlayToEndTime,
                                               object: self.player?.currentItem)
    }
    
    @objc func playerItemDidReachEnd(notification: Notification) {
        if let playerItem = notification.object as? AVPlayerItem {
            self.player?.seek(to: .zero)
            self.player?.play()
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.player?.play()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.player?.pause()
    }
    
//    init(videoURL: String) {
//        self.videoURL = videoURL
//        super.init(nibName: nil, bundle: nil)
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("I WILL NEVER instantiate through storyboard! It's impossible to initialize super.init?(coder aDecoder: NSCoder) with any other parameter")
//    }
}
